pgn.lua
=======
PGN (Portable Game Notation) files may contain one or more games
This module should output each game as a table of (non-validated) moves
Supports:
 - nested variations
 - annotations
TODO:
 - search for game/position/player
 - fast search for large PGN files
 - search positions from PGN
 - IOCC country codes
 - NAG codes
 - find duplicate games

iocc.txt
========
International country codes used in the PGN format

nag.txt
=======
Numeric annotation codes used in the PGN format

fen.lua
=======
FEN is one way to serialize the state of the board
Supports:
 - FEN => board and board => FEN

board.lua
=========
Lua representation of the board
Supports:
 - move validation
 - list of available moves (to/from/all)
 - SAN move format (WIP)
TODO:
 - evaluate position
 - search

Future:

fics.lua
========
Backend for internet chess servers (ICS) using LuaSocket

ai.lua
======
Backend for finding the "best" move, hints, etc

book.lua
========
Backend for opening books
Get the ECO code for openings, and the name of the opening (hopefully localized)
