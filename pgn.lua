local pgn = {}

local match = string.match
local gmatch = string.gmatch
local sub = string.sub
local len = string.len
local byte = string.byte
local find = string.find
--local upper = string.upper
--local format = string.format

function pgn.moves(s, i)
  local moves = {}
  local n = 0
  moves.Annotations = {}
  local i = i or 1
  while i <= len(s) do
    local b = byte(s, i)
    if b == 40 then -- (
      -- start of variation
      i = i + 1
      n = n + 1
      moves[n], i = pgn.moves(s, i)
    elseif b == 41 then -- )
      -- end of variation
      i = i + 1
      return moves, i
    elseif b == 123 then -- {
      -- annotation
      local j = find(s, "}", i + 1, true)
      assert(j, "unfinished annotation")
      moves.Annotations[n] = sub(s, i + 1, j - 1)
      i = j
    elseif b >= 49 and b <= 57 then -- 1-9
      -- move number indication
      local n2, ws = match(s, "^(%d+)[%.]*(%s*)", i)
      i = i + len(n2) + len(ws or "")
    else
      -- SAN move
      local san = match(s, "^([%a][%w%+%=%#%-]+)", i)
      if san then
        i = i + len(san)
        n = n + 1
        moves[n] = san
      else
        i = i + 1
      end
    end
  end
  return moves, i
end

-- PGN text lines are limited to having a maximum of 255 characters per line
-- including the newline character.
-- Lines with 80 or more printing characters are strongly discouraged
-- because of the difficulties experienced by common text editors with long lines.

--[[
Seven Tag Roster
================
1.Event: the name of the tournament or match event.
2.Site: the location of the event. This is in "City, Region COUNTRY" format, where COUNTRY is the three-letter International Olympic Committee code for the country. An example is "New York City, NY USA".
3.Date: the starting date of the game, in YYYY.MM.DD form. "??" are used for unknown values.
4.Round: the playing round ordinal of the game within the event.
5.White: the player of the white pieces, in "last name, first name" format.
6.Black: the player of the black pieces, same format as White.
7.Result: the result of the game. This can only have four possible values: "1-0" (White won), "0-1" (Black won), "1/2-1/2" (Draw), or "*" (other, e.g., the game is ongoing).
]]

--local str = { "Event", "Site", "Date", "Round", "White", "Black", "Result" }

-- scan PGN file without parsing the moves
function pgn.scan(s)
  local scan = {}
  local n = 0
  local game = nil
  if sub(s, -1) ~= "\n" then
    s = s .. "\n"
  end
  for line in gmatch(s, "(.-)\n") do
    -- extract tag and value
    local tag, value = match(line, '%[(%w+) "(.*)"%]')
    if tag and value then
      if game == nil then
        game = {}
        n = n + 1
        scan[n] = game
      end
      -- line contains tag pair
      game[tag] = value
    else
      -- line contains movetext
      scan[n].Movetext = (scan[n].Movetext or "") .. line .. "\n"
      game = nil
    end
  end
  return scan
end

return pgn