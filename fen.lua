-- Piece type 0-12 (4 bits)
local piecesB =
{
  ['-'] = 0,
  P = 1, p = 2,
  N = 3, n = 4,
  B = 5, b = 6,
  R = 7, r = 8,
  Q = 9, q = 10,
  K = 11, k = 12
}
local pieces =
{
  [0] = '-',
  'P', 'p',
  'N', 'n',
  'B', 'b',
  'R', 'r',
  'Q', 'q',
  'K', 'k'
}

-- Side to move 0-1 (2 bits)
local sidesB =
{
  b = 0, w = 1
}
local sides =
{
  [0] = 'b', 'w'
}

-- Castling rights 0-16 (4 bits)
-- K = 1, Q = 2, k = 4, q = 8
-- csa%(p + p) >= p
local csaB =
{
  ['-'] = 0,
  K = 1, Q = 2, KQ = 3,
  k = 4, Kk = 5, Qk = 6,
  KQk = 7, q = 8, Kq = 9,
  Qq = 10, KQq = 11, kq = 12,
  Kkq = 13, Qkq = 14, KQkq = 15
}
local csa =
{
  [0] = '-',
  'K', 'Q', 'KQ',
  'k', 'Kk', 'Qk',
  'KQk', 'q', 'Kq',
  'Qq', 'KQq', 'kq',
  'Kkq', 'Qkq', 'KQkq'
}

-- Squares 0-64 (6 bits)
local squaresB =
{
  ['-'] = 0,
  a1 = 1, b1 = 2, c1 = 3, d1 = 4, e1 = 5, f1 = 6, g1 = 7, h1 = 8,
  a2 = 9, b2 = 10, c2 = 11, d2 = 12, e2 = 13, f2 = 14, g2 = 15, h2 = 16,
  a3 = 17, b3 = 18, c3 = 19, d3 = 20, e3 = 21, f3 = 22, g3 = 23, h3 = 24,
  a4 = 25, b4 = 26, c4 = 27, d4 = 28, e4 = 29, f4 = 30, g4 = 31, h4 = 32,
  a5 = 33, b5 = 34, c5 = 35, d5 = 36, e5 = 37, f5 = 38, g5 = 39, h5 = 40,
  a6 = 41, b6 = 42, c6 = 43, d6 = 44, e6 = 45, f6 = 46, g6 = 47, h6 = 48,
  a7 = 49, b7 = 50, c7 = 51, d7 = 52, e7 = 53, f7 = 54, g7 = 55, h7 = 56,
  a8 = 57, b8 = 58, c8 = 59, d8 = 60, e8 = 61, f8 = 62, g8 = 63, h8 = 64
}
local squares =
{
  [0] = '-',
  'a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1',
  'a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2',
  'a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3',
  'a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4',
  'a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5',
  'a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6',
  'a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7',
  'a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'
}

local fen = {}

local len = string.len
local byte = string.byte
local sub = string.sub
local gmatch = string.gmatch
local char = string.char
local concat = table.concat
local upper = string.upper
local format = string.format

--- Parses Forsyth�Edwards Notation
-- @param fen FEN notation
-- @return Board table
function fen.board(fen)
  -- break string into sections
  if sub(fen, -1) ~= " " then
    fen = fen .. " "
  end
  local s = {}
  for sz in gmatch(fen, "(.-) ") do
    s[#s + 1] = sz
  end
  local board = {}
  -- 1.Piece placement
  local s1 = s[1]
  if s1 then
    local x = 1
    local y = 8
    for i = 1, len(s1) do
      local b = byte(s1, i)
      if b >= 49 and b <= 56 then -- 1-8
        -- empty square
        x = x + (b - 48)
      elseif b == 47 then -- /
        -- rank
        x = 1
        y = y - 1
      elseif b == 32 then -- space
        -- end of section
        break
      else
        -- piece
        local c = char(b)
        --if not white[c] and not black[c] then
          --error("invalid symbol:" .. c)
        --end
        local sq = x + (y - 1)*8
        local p = piecesB[c or 0]
        board[sq] = p
        if p == 11 then
          board.wks = sq
        elseif p == 12 then
          board.bks = sq
        end
        x = x + 1
      end
    end
  end
  -- 2.Side to move
  board.stm = sidesB[s[2] or 0]
  -- 3.Castling ability
  board.csa = csaB[s[3] or 0]
  -- 4.En Passant square
  board.eps = squaresB[s[4] or 0]
  -- 5.Halfmoves
  local s5 = s[5]
  local hmc = tonumber(s[5] or 0) or 0
  if hmc < 0 then
    hmc = -hmc
  end
  board.hmc = hmc
  -- 6.Fullmove counter
  local fmc = tonumber(s[6] or 0) or 0
  if fmc < 0 then
    fmc = -fmc
  end
  fmc = fmc*2
  if board.stm == sidesB.b then
    fmc = fmc + 1
  end
  board.ply = fmc
  return board
end

--- Generates Forsyth�Edwards Notation
-- @param board Board table
-- @return FEN notation
function fen.string(board)
  local fen = {}
  local n = 0
  -- 1.Piece placement
  for y = 8, 1, -1 do
    for x = 1, 8 do
      local i = x + (y - 1)*8
      local p = board[i]
      if p then
        n = n + 1
        fen[n] = pieces[p]
      else
        local x = fen[n]
        if type(x) == "number" then
          fen[n] = x + 1
        else
          n = n + 1
          fen[n] = 1
        end
      end
    end
    if y > 1 then
      n = n + 1
      fen[n] = "/"
    end
  end
  -- 2.Side to move
  fen[n + 1] = " " .. sides[board.stm]
  -- 3.Castling
  fen[n + 2] = " " .. csa[board.csa]
  -- 4.En Passant
  fen[n + 3] = " " .. squares[board.eps]
  -- 5.Halfmoves
  fen[n + 4] = " " .. board.hmc
  -- 6.Fullmove count
  local fmc = board.ply
  if board.stm == sidesB.b then
    fmc = fmc + 1
  end
  fmc = fmc/2
  fen[n + 5] = " " .. fmc
  return concat(fen)
end

local b0 = string.byte('1') - 1
local bA = string.byte('a') - 1

function fen.ascii(board)
  local sz = {}
  local n = 1
  sz[n] = " abcdefgh \n"
  for y = 8, 1, -1 do
    n = n + 1
    sz[n] = string.char(y + b0)
    for x = 1, 8 do
      local i = x + (y - 1)*8
      local p = board[i]
      n = n + 1
      sz[n] = pieces[p or 0]
      if i%8 == 0 then
        n = n + 1
        sz[n] = "\n"
      end
    end
  end
  return concat(sz)
end

function fen.san(board, moves)
  local san = {}
  local n = 0
  for i = 1, #moves, 2 do
    local from = moves[i]
    local to = moves[i + 1]
    local p1 = board[from]
    local p2 = board[to]
    local move = ''
    -- 0.Castling
    local offset = to - from
    if p1 >= 11 and (offset == -2 or offset == 2) then
      if offset == -2 then
        move = 'O-O-O'
      elseif offset == 2 then
        move = 'O-O'
      end
    else
      -- 1.Piece type
      local s1 = ''
      if p1 >= 3 then
        s1 = upper(pieces[p1])
      end
      -- 2.Source
      local s2 = ''
      if p1 <= 2 then
        if p2 or to == board.eps then
          s2 = sub(squares[from], 1, 1)
        end
      elseif p1 <= 10 then
        -- todo: disambiguation
        local a, f, r = false, false, false
        local file = (from - 1)%8
        local rank = (from - 1 - file)/8
        for j = 1, #moves, 2 do
          local from2 = moves[j]
          local to2 = moves[j + 1]
          if from ~= from2 and to == to2 and p1 == board[from2] then
            local file2 = (from2 - 1)%8
            local rank2 = (from2 - 1 - file2)/8
            a = true
            if file == file2 then
              r = true
            elseif rank == rank2 then
              f = true
            end
          end
        end
        if a then
          if f and r then
            s2 = squares[from]
          elseif r then
            s2 = sub(squares[from], 2, 2)
          else
            s2 = sub(squares[from], 1, 1)
          end
        end
      end
      -- 3.Capture
      local s3 = ''
      if p2 or (p1 <= 2 and to == board.eps) then
        s3 = 'x'
      end
      -- 4.Destination
      local s4 = squares[to]
      -- 5.Promotion
      local s5 = ''
      if p1 <= 2 then
        if to <= 8 or to >= 56 then
          s5 = '=' .. (pieces[p3] or 'Q')
        end
      end
      move = format("%s%s%s%s%s", s1, s2, s3, s4, s5)
    end
    n = n + 1
    san[n] = move
  end
  return san
end

return fen