--- Lookup data and indexes

-- initial position
local inital64 =
{
  7,  3,  5,  9, 11,  5,  3,  7,
  1,  1,  1,  1,  1,  1,  1,  1,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,
  2,  2,  2,  2,  2,  2,  2,  2,
  8,  4,  6, 10, 12,  6,  4,  8
}

-- 10x12 indexing
local mailbox120 =
{
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1,  1,  2,  3,  4,  5,  6,  7,  8, -1,
  -1,  9, 10, 11, 12, 13, 14, 15, 16, -1,
  -1, 17, 18, 19, 20, 21, 22, 23, 24, -1,
  -1, 25, 26, 27, 28, 29, 30, 31, 32, -1,
  -1, 33, 34, 35, 36, 37, 38, 39, 40, -1,
  -1, 41, 42, 43, 44, 45, 46, 47, 48, -1,
  -1, 49, 50, 51, 52, 53, 54, 55, 56, -1,
  -1, 57, 58, 59, 60, 61, 62, 63, 64, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
}

-- 8x8 indexing
local mailbox64 =
{
  22, 23, 24, 25, 26, 27, 28, 29,
  32, 33, 34, 35, 36, 37, 38, 39,
  42, 43, 44, 45, 46, 47, 48, 49,
  52, 53, 54, 55, 56, 57, 58, 59,
  62, 63, 64, 65, 66, 67, 68, 69,
  72, 73, 74, 75, 76, 77, 78, 79,
  82, 83, 84, 85, 86, 87, 88, 89,
  92, 93, 94, 95, 96, 97, 98, 99
}

-- attack offsets for each piece
local attacks120 =
{
  { 9, 11 },
  { -11, -9 },
  { -21, -19, -12, -8, 8, 12, 19, 21 },
  { -21, -19, -12, -8, 8, 12, 19, 21 },
  { -11, -9, 9, 11 },
  { -11, -9, 9, 11 },
  { -10, -1, 1, 10 },
  { -10, -1, 1, 10 },
  { -11, -10, -9, -1, 1, 9, 10, 11 },
  { -11, -10, -9, -1, 1, 9, 10, 11 },
  { -11, -10, -9, -1, 1, 9, 10, 11 },
  { -11, -10, -9, -1, 1, 9, 10, 11 }
}

-- advance offsets for pawns
local advances64 = { 8, -8 }

-- sliding ability for each piece
local sliding = { 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 }

-- number of rays for each piece
local rays = { 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 0, 0 }

-- current board
local board
-- generated moves
local moves
-- number of generated moves
local n = 0
-- side to move
local side1, side2
-- king and en passant squares, castling availability
local ksq, eps, csa
-- check
local check
-- pins (if not in check)
local pins = {}

--- Helper functions

--- Checks if a square is attacked
-- @param stm Side to move
-- @param to Square number between 1 and 64
-- @return First attacking square found
local function attacked(stm, to)
  local to120 = mailbox64[to]
  --assert(to120, "invalid to square")
  -- for each piece type
  for p1 = 2 - stm, 12, 2 do
    local offsets = attacks120[p1]
    --assert(offsets, "invalid piece on the board")
    -- for each offset
    local stop = rays[p1] == 0
    for i = 1, #offsets do
      local offset120 = offsets[i]
      local from120 = to120
      repeat
        from120 = from120 - offset120
        local from = mailbox120[from120]
        -- off the board
        if from == -1 then
          break
        end
        local p2 = board[from]
        if p2 == p1 then
          return from
        end
        -- blocked
      until p2 or stop
    end
  end
end

--- Move generation functions

--- Prepares move generation
-- @param board Board table
-- @param moves Moves list
local function genInit(_board, _moves)
  board = _board
  moves = _moves or {}
  n = #moves + 1
  -- en passant square
  eps = _board.eps
  -- side to move
  side1 = _board.stm
  side2 = (side1 + 1)%2
  -- king square
  if side1 == 1 then
    ksq = _board.wks
  else
    ksq = _board.bks
  end
  -- castling availability
  if side1 == 1 then
    csa = _board.csa%4
  else
    csa = (_board.csa - csa)/4
  end
  -- position starts in check?
  check = attacked(side2, ksq)
  -- reset pins
  for on in pairs(pins) do
    pins[on] = nil
  end
  -- finds pinned pieces in a non-check position (internal)
  if not check then
    -- test if removing each piece results in check
    for on = 1, 64 do
      local p1 = board[on]
      if p1 and p1 > 0 and p1%2 == side1 then
        board[on] = nil
        if attacked(side2, ksq) then
          pins[on] = true
        end
        board[on] = p1
      end
    end
  end
end

--- Generate move without testing if the moving piece is pinned
local function genMoveSafe(from, to)
  moves[n], moves[n + 1] = from, to
  n = n + 2
end

--- Generate move
local function genMoveCheck(from, to, p1, p2)
  --assert(board[from] == p1)
  --assert(board[to] == p2)
  local ksq2 = ksq
  if ksq == from then
    ksq2 = to
  end
  -- test if the resulting position is check for the moving side
  board[from], board[to] = nil, p1
  if not attacked(side2, ksq2) then
    moves[n], moves[n + 1] = from, to
    n = n + 2
  end
  board[from], board[to] = p1, p2
end

--- Generates one move
-- @param from Starting square
-- @param to Ending square
-- @param p1 Piece on the starting square
-- @param p2 Piece on the ending square
local function genMove(from, to, p1, p2)
  -- todo: not sure if pinned pawns work with En Passant
  if check or pins[from] or p1 >= 11 then
    genMoveCheck(from, to, p1, p2)
  else
    genMoveSafe(from, to)
  end
end

-- Generates moves to a given square
-- @param from Ending square
-- @param p1 Piece type or 0 for any
local function genMovesTo(to, _p1)
  local to120 = mailbox64[to]
  --assert(to120, "invalid to square")
  local p, k = _p1, _p1
  if _p1 == 0 then
    p, k = 2 - side1, 12 - side1
  end
  --assert(p%2 == side1)
  local p2 = board[to]
  -- attacking own piece
  if p2 and p2%2 == side1 then
    return
  end
  -- pawn advance moves
  local advance = advances64[2 - side1]
  if p <= 2 and p2 == nil then
    --assert(from > 8 and from <= 56, "invalid pawn square")
    local from1 = to - advance
    if board[from1] == p then
      -- advance one
      genMove(from1, to, p)
    elseif board[from1] == nil then
      local from2 = from1 - advance
      if p == inital64[from2] then
        if board[from2] == p then
          -- advance two
          genMove(from2, to, p)
        end
      end
    end
  end
  -- attack moves
  for p1 = p, k, 2 do
    local offsets = attacks120[p1]
    --assert(offsets, "invalid piece on the board")
    -- for each offset
    local stop = sliding[p1] == 0
    for i = 1, #offsets do
      local offset120 = offsets[i]
      local from120 = to120
      repeat
        from120 = from120 - offset120
        local from = mailbox120[from120]
        -- off the board
        if from == -1 then
          break
        end
        local p1b = board[from]
        if p1b then
          -- capture
          if p1b == p1 then
            if p1 <= 2 and p2 == nil then
              if to == eps then
                -- en passant capture
                local csq = eps + advance/2
                local p3 = board[csq]
                board[csq] = nil
                genMove(from, to, p1)
                board[csq] = p3
              end
              break
            else
              genMove(from, to, p1, p2)
            end
          end
          -- blocked
          break
        end
      until stop
    end
  end
end

-- Generates moves for one piece
-- @param from Starting square
-- @param p1 Piece on the starting square
local function genMovesFrom(from, p1)
  local advance = advances64[2 - side1]
  local from120 = mailbox64[from]
  -- pawn advance moves
  if p1 <= 2 then
    --assert(from > 8 and from <= 56, "invalid pawn square")
    local to1 = from + advance
    if board[to1] == nil then
      -- advance one
      genMove(from, to1, p1)
      if p1 == inital64[from] then
        local to2 = to1 + advance
        if board[to2] == nil then
          -- advance two
          genMove(from, to2, p1)
        end
      end
    end
  end
  -- attack moves
  local stop = sliding[p1] == 0
  local offsets = attacks120[p1]
  --assert(offsets, "invalid piece on the board")
  -- for each offset
  for i = 1, #offsets do
    local to120 = from120
    local offset120 = offsets[i]
    repeat
      to120 = to120 + offset120
      local to = mailbox120[to120]
      -- off the board
      if to == -1 then
        break
      end
      local p2 = board[to]
      if p2 then
        -- own piece
        if side1 ~= p2%2 then
          -- capture
          genMove(from, to, p1, p2)
        end
        break
      else
        if p1 <= 2 then
          -- pawn
          if to == eps then
            -- en passant capture
            local csq = eps + advance/2
            local p3 = board[csq]
            board[csq] = nil
            genMove(from, to, p1)
            board[csq] = p3
          end
          break
        else
          -- quiet move
          genMove(from, to, p1)
        end
      end
    until stop
  end
end

-- Generates castling moves, based on availability
local function genMovesCastle()
  -- king in place
  local k = board[ksq]
  if k ~= inital64[ksq] then
    return
  end
  -- not in check
  if check then
    return
  end
  -- king side
  if csa == 1 or csa == 3 then
    -- rook in place
    local rksq = ksq + 3
    if board[rksq] == inital64[rksq] then
      -- blocked
      if not (board[ksq + 1] or board[ksq + 2]) then
        -- passes through check
        if not attacked(side2, ksq + 1) then
          genMove(ksq, ksq + 2, k)
        end
      end
    end
  end
  -- queen side
  if csa == 2 or csa == 3 then
    -- rook in place
    local rqsq = ksq - 4
    if board[rqsq] == inital64[rqsq] then
      -- blocked
      if not (board[ksq - 1] or board[ksq - 2] or board[ksq - 3]) then
        -- passes through check
        if not attacked(side2, ksq - 1) then
          genMove(ksq, ksq - 2, k)
        end
      end
    end
  end
end

--- Generates all moves for a given side
local function genMovesAll()
  -- for each piece on the board
  for from = 1, 64 do
    local p1 = board[from]
    if p1 and p1%2 == side1 then
      genMovesFrom(from, p1)
    end
  end
end

-- External functionality
local gen = {}

--- Get all moves to a given square
-- @param board Board table
-- @param to Square number
-- @param p1 Piece type (optional)
-- @param moves Moves table (optional)
-- @return Moves table
function gen.to(_board, to, p1, _moves)
  if p1 and p1 > 0 and p1%2 ~= _board.stm then
    return
  end
  genInit(_board, _moves)
  if p1 == nil or p1 >= 11 then
    -- include castling
    if _board.stm == 1 then
      if to == 3 or to == 7 then
        genMovesCastle()
      end
    else
      if to == 59 or to == 63 then
        genMovesCastle()
      end
    end
  end
  genMovesTo(to, p1 or 0)
  return moves
end

--- Get all moves from a given square
-- @param board Board table
-- @param from Square number
-- @param moves Moves table (optional)
-- @return Moves table
function gen.from(_board, from, _moves)
  local p1 = _board[from]
  if p1 == nil or p1 == 0 or p1%2 ~= _board.stm then
    return
  end
  genInit(_board, _moves)
  if p1 >= 11 then
    -- include castling
    genMovesCastle()
  end
  genMovesFrom(from, p1)
  return moves
end

--- Get all moves
-- @param board Board table
-- @param moves Moves table (optional)
-- @return Moves table
function gen.all(_board, _moves)
  genInit(_board, _moves)
  genMovesCastle()
  genMovesAll()
  return moves
end

return gen