local concat = table.concat

local format = string.format
local gsub = string.gsub
local match = string.match
local rep = string.rep

-- Lua keywords, disallowed for use as table keys
local keywords =
{
  ["and"] = true, ["break"] = true, ["do"] = true,
  ["else"] = true, ["elseif"] = true, ["end"] = true,
  ["false"] = true, ["for"] = true, ["function"] = true,
  ["if"] = true, ["in"] = true, ["local"] = true,
  ["nil"] = true, ["not"] = true, ["or"] = true,
  ["repeat"] = true, ["return"] = true, ["then"] = true,
  ["true"] = true, ["until"] = true, ["while"] = true
}

-- String representation of infinities
-- see Robin's "ser" library: https://github.com/gvx/Ser
local infinities =
{
  [tostring(1/0)] = '1/0', -- 1.#INF or math.huge
  [tostring(-1/0)] = '-1/0', -- -1.#INF or -math.huge
  [tostring(0/0)] = '0/0' -- 1.#IND or NaN
}

-- Returns a double quoted string
local function toQuotedString(sz)
  -- handle double quotes and new line
  -- backslash (hex 5C)
  sz = gsub(sz, '\\', '\\\\')
  -- double quote character (hex 22)
  sz = gsub(sz, '"', '\\\"')
  -- newline (hex 0A)
  sz = gsub(sz, '\n', '\\n')
  -- carriage return (hex 0D) becomes \r
  sz = gsub(sz, '\r', '\\r')
  -- todo: nul character (hex 00) becomes \000
  sz = format('"%s"', sz)
  -- todo: format "%q" is a little funny about new lines
  --sz = format('%q', sz)
  return sz
end

local indent = false
local function toString(t, i, compact)
  -- keep track of numeric indexes
  local n = 1
  -- list of key and value pairs
  local t2 = {}
  for k, v in pairs(t) do
    -- numeric index?
    if n and n == k then
      n = n + 1
    else
      n = nil
    end
    -- value
    local vt = type(v)
    if vt == "table" then
      v = toString(v, i + 1, n)
    elseif vt == "string" then
      v = toQuotedString(v)
    elseif vt == "number" then
      v = tostring(v)
      v = infinities[v] or v
    else
      v = tostring(v)
    end
    local pair = v
    if n == nil then
      -- key
      local kt = type(k)
      if kt == "string" then
        if keywords[k] or not match(k, '^[%a_][%w_]*$') then
          k = toQuotedString(k)
          k = format('[%s]', k)
        end
      elseif kt == "number" then
        k = tostring(k)
        k = infinities[k] or k
        k = format('[%s]', k)
      else
        k = tostring(k)
        k = format('[%s]', k)
      end
      -- key and value pair
      pair = format("%s=%s", k, v)
    end
    -- output
    t2[#t2 + 1] = pair
  end
  if indent and #t2 > 0 then
    -- indented
    local f1 = rep("  ", i)
    local f2 = rep("  ", i + 1)
    local tsz = f2 .. concat(t2, ",\n" .. f2)
    -- brackets
    if compact or i == 0 then
      return format("{\n%s\n%s}", tsz, f1)
    else
      return format("\n%s{\n%s\n%s}", f1, tsz, f1)
    end
  else
    -- non-indented
    local tsz = concat(t2, ",")
    return format("{%s}", tsz)
  end
end

-- todo:
-- slow and creates a lot of intermediate strings
-- non-sorted for non-numeric keys with gaps
-- does not handle cycles
-- tables, functions or userdata keys are converted using "tostring"

--- Converts table to a string representation
--- Makes the string as compact as possible
--- Handles boolean and special keywords as keys
-- @param t Table
-- @param c Format style, either "compact" or "readable" (optional)
-- @param i Indentation level (internal)
-- @return String
function table.tostring(t, c)
  if c == nil then
    c = "readable"
  end
  indent = (c == "readable")
  return toString(t, 0)
end

--- Converts string to a table
-- @param sz String
-- @return Table
function table.fromstring(sz)
  -- optional: type checking
  --assert(type(sz) == "string", "input is not a string")
  -- todo: highly unsafe
  local r, e = pcall(loadstring, "return " .. sz)
  if r then
    r, e = pcall(e)
    if r then
      return e
    end
  end
  return nil, e
end