-- 
-- Random vs Random chess game
--

display:create("terminal test", 1024, 768, 32, true)

terminal = require('utils.log.terminal')

local chess = require("chess.board")
local fen = require("chess.fen")
--local serialize = require("chess.serialize")

local profiler = require('utils.log.profile')
profiler.hookall()
profiler.setclock(system.get_ticks)

board = fen.board("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0")
moves = {}

local function newG()
  --board = fen.board("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0")
  board = fen.board("rnbqkbnr/pppppppp/8/8/8/8/QQQQQQQQ/RNBQKBNR w KQkq - 0 0")
  moves = chess.getMoves(board)
end

local function moveN(N)
  math.randomseed(system.get_ticks())
  profiler.reset()
  profiler.start()
  moves = {}
  for i = 1, N do
    moves = chess.getMoves(board)
    if #moves == 0 then
      newG()
      --break
    end

    -- ignore captures
    -- so can test with more pieces
    for i = #moves - 1, 2, -2 do
      if #moves == 2 then
        break
      end
      if board[moves[i + 1] ] then
        table.remove(moves, i)
        table.remove(moves, i)
      end
    end

    -- get a random move
    local n = #moves/2
    local r = 1
    if n > 1 then
      r = math.random(1, n)*2 - 1
    end
    chess.move(board, moves[r], moves[r + 1])
  end
  profiler.stop()
end
require("utils.table.util")
require("chess.example.table")
local function status()
  terminal.clear()
  terminal.print('Board:')
  terminal.print('')
  terminal.print(fen.ascii(board))
  terminal.print('')
  terminal.printf('FEN:%s', fen.string(board))
  terminal.printf('Side:%s', board.stm)
  terminal.printf('En Passant:%s', board.eps)
  terminal.printf('Halfmoves:%d', board.hmc)
  terminal.printf('Ply:%d', board.ply)
--[[
  local bksq = chess.moves.getKSQ(board, 0)
  local wksq = chess.moves.getKSQ(board, 1)
  terminal.printf('wKSQ:%s', wksq)
  terminal.printf('bKSQ:%s', bksq)
  local wc = chess.moves.isAttacked(board, 0, wksq)
  local bc = chess.moves.isAttacked(board, 1, bksq)
  wc = wc and "yes" or "no"
  bc = bc and "yes" or "no"
  terminal.printf('wcheck:%s', wc)
  terminal.printf('bcheck:%s', bc)
  ]]
  --[[
  terminal.print(chess.moves.validatePosition(board, board.stm, board.eps, board.csa))
  terminal.print(chess.moves.validateMaterial(board))
  ]]

  terminal.print('Available moves:')

  if #moves == 0 then
    terminal.print("GAME OVER!")
  end

  moves = chess.getMoves(board)
  smoves = fen.san(board, moves)
  local out = {}
  for i = 1, #smoves do
    --[[
    local to = moves[i + 1]
    local n2 = chess.toSAN(board, from, to)
    smoves[#smoves + 1] = n2
    local n3 = chess.toAlgebraic(board, from, to)
    local n4 = chess.toLAN(board, from, to)
    local n5 = chess.pgn.iccn(board, from, to)
    out[#out + 1] = string.format("%d.%s (%s) (%s) (%s)", n, n2, n3, n4, n5)
    ]]
    out[#out + 1] = string.format("%d.%s ", i, smoves[i])
    
    if i%6 == 0 then
      out[#out + 1] = "\n"
    end
  end
  terminal.print(table.concat(out))
  if table.ucount(smoves) ~= #smoves then
    lookup = {}
    for i = 1, #smoves do
      lookup[smoves[i] ] = (lookup[smoves[i] ] or 0) + 1
    end
    out = {}
    for i,v in pairs(lookup) do
      if v > 1 then
        out[i] = v
      end
    end
    error(table.tostring(out))
  end

  terminal.print('Profiling data:')
  local n = 1
  for f, c, t, d in profiler.query("time", 10) do
    terminal.printf("%d.'%s'x%d time:%s avg:%s (%s)", n, f, c, t, t/c, d)
    n = n + 1
  end
end

function mouse:on_press(b)
  if b == 108 then
    moveN(1)
  elseif b == 110 then
    newG()
  elseif b == 109 then
    moveN(1000)
  end
  status()
end

newG()
status()

math.randomseed(os.time())
for i = 1, 64 do
  math.random()
end