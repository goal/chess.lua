local chess = require("chess.board")

local model = {}
local mt = { __index = model }

function model:new()
  local self = { b = chess.new() }
  return setmetatable(self, mt)
end

function model:getPosition(sq)
  if sq < 1 or sq > 64 then
    return
  end
  sq = sq - 1
  local r = sq%8
  local f = 7 - (sq - r)/8
  return r, f
end

function model:getSquare(x, y)
  if x < 0 or x > 8 or y < 0 or y > 8 then
    return
  end
  local r = math.floor(x)
  local f = 7 - math.floor(y)
  return r + f*8 + 1
end

function model:move(from, to)
  if from and to then
    if chess.moves.legal(self.b, from, to) then
      chess.move(self.b, from, to)
      return true
    end
  end
  return false
end

function model:grabPiece(from)
  local p = self.b[from]
  if p then
    self:dropPiece(self.from)
    self.from = from
    self.x, self.y = self:getPosition(from)
    return p
  end
end

function model:dragPiece(dx, dy)
  if self.from then
    self.x, self.y = self.x + dx, self.y + dy
    local cx, cy = self.x + 0.5, self.y + 0.5
    self.to = self:getSquare(cx, cy)
    return self.to
  end
end

function model:dropPiece(to)
  if self.from then
    if self:move(self.from, to or self.to) then
      local moves = chess.getMoves(self.b)
      if #moves > 0 then
        local r = math.random(#moves/2)
        self:move(moves[r*2 - 1], moves[r*2])
      end
    end
    self.from = nil
    self.x, self.y = nil, nil
    self.to = nil
  end
end

function model:getPiece(sq)
  sq = sq or self.from
  local x, y = self:getPosition(sq)
  if sq == self.from then
    x, y = self.x, self.y
  end
  return self.b[sq], x, y
end

return model