local model = require("model")
local game = model:new()

local view = require("view")
local default = view:new(64*8, 64*8)
default:load("ChessAlpha2.ttf", "iIkKjJlLmMnN")

local control = require("control")
control.setObject(game)

function love.mousepressed()
  local x, y = love.mouse.getPosition()
  x, y = default:toLocal(x, y)
  control.press(x, y)
end
function love.mousemoved()
  local x, y = love.mouse.getPosition()
  x, y = default:toLocal(x, y)
  control.move(x, y)
end
function love.mousereleased()
  control.release()
end

function love.draw()
	default:draw(game)
end

function love.update(dt)
end