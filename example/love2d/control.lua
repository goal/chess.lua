local control = {}

local _object

function control.setObject(object)
  _object = object
end

local x0, y0
local sq

function control.press(x1, y1)
  local from = _object:getSquare(x1, y1)
  if from then
    _object:grabPiece(from)
    x0, y0 = x1, y1
  end
end

function control.move(x1, y1)
  if x0 and y0 then
    local dx, dy = x1 - x0, y1 - y0
    sq = _object:dragPiece(dx, dy)
    x0, y0 = x1, y1
  end
end

function control.release(x1, y1)
  if x0 and y0 then
    _object:dropPiece(sq)
    x0, y0 = nil, nil
  end
end

return control