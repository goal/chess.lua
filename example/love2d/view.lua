local view = {}
local mt = { __index = view }

function view:new(w, h)
  local self = { w = w, h = h }
  return setmetatable(self, mt)
end

function view:load(fn, sz)
  self.font = love.graphics.newFont(fn, self.w/8)
  assert(self.font)
  if self.font then
    self.m = {}
    for i = 1, #sz do
      local c = string.sub(sz, i, i)
      self.m[i] = c
    end
  end
end

function view:toLocal(x, y)
  return x/self.w*8, y/self.h*8
end

function view:draw(object)
  local w, h = self.w, self.h
  local sw, sh = w/8, h/8
  love.graphics.setColor(64, 64, 64, 255)
  love.graphics.rectangle('fill', 0, 0, w, h)
  love.graphics.setColor(128, 128, 128, 255)
  for r = 0, 7 do
    for f = 0, 7 do
      local ox, oy = r*sw, f*sh
      if (r + f)%2 ~= 0 then
        love.graphics.rectangle('fill', ox, oy, sw, sh)
      end
    end
  end
  -- draw pieces
  love.graphics.setFont(self.font)
  local fh = self.font:getHeight()
  for sq = 1, 64 do
    local p, ox, oy = object:getPiece(sq)
    ox, oy = ox*sw, oy*sh
    if object.b.eps == sq then
      love.graphics.setColor(0, 255, 0, 128)
      love.graphics.rectangle('fill', ox, oy, sw, sh)
    end
    if p then
      if p%2 == 0 then
        love.graphics.setColor(0, 0, 0, 255)
      else
        love.graphics.setColor(255, 255, 255, 255)      
      end
      local c = self.m[p]
      oy = oy + sh/2 - fh/2
      love.graphics.printf(c, ox, oy, sw, "center")
    end
  end
end

return view