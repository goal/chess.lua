local chess = {}

chess.gen = require("chess.gen")
--chess.pgn = require("chess.pgn")
--chess.fen = require("chess.fen")

local char = string.char
local floor = math.floor

--local serialize = require("chess.serialize")

-- 57 58 59 60 61 62 63 64
-- 49 50 51 52 53 54 55 56
-- 41 42 43 44 45 46 47 48
-- 33 34 35 36 37 38 39 40
-- 25 26 27 28 29 30 31 32
-- 17 18 19 20 21 22 23 24
-- 9  10 11 12 13 14 15 16
-- 1  2  3  4  5  6  7  8

--[[
function chess.new(fen)
  fen = fen or "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 0"
  return chess.fen.board(fen)
end
]]

function chess.getMoves(b)
  local moves = {}
  -- for each piece
  chess.gen.all(b, moves)
  assert(#moves%2 == 0, 'odd number of move pairs')
  return moves
end

--[[
function chess.toAlgebraic(b, from, to, p3)
  return chess.pgn.algebraic(b, from, to, p3)
end

function chess.toLAN(b, from, to, p3)
  return chess.pgn.lan(b, from, to, p3)
end

function chess.toSAN(b, from, to, p3)
  --local l, f, r = chess.moves.legal(b, from, to)
  return chess.pgn.san(b, from, to, p3)
end

function chess.fromSAN(b, san)
  local moves = chess.getMoves(b)
  for i = 1, #moves, 2 do
    local from = moves[i]
    local to = moves[i + 1]
    local san2 = chess.toSAN(b, from, to)
    if san == san2 then
      return from, to
    end
  end
end
]]

--- Makes a move and updates the state of the board,
-- without checking if the move is legal
function chess.move(board, from, to, promo)
  --assert(from >= 1 and from <= 64, "invalid from square")
  --assert(to >= 1 and to <= 64, "invalid to square")
  --assert(to ~= from, "move squares are the same")
  
  local p1 = board[from]
  local p2 = board[to]
  
  --assert(p1, "starting square is empty")
  --assert(p2 == nil or p2 <= 10, "cannot capture king")

  -- 2.Side to move
  local stm1 = board.stm
  local stm2 = (stm1 + 1)%2
  board.stm = stm2
  -- 3.Castling availability
  local csa = board.csa
  local wcsa = csa%4
  local bcsa = (csa - wcsa)/4
  if to == 8 or from == 8 then
    -- white king-side
    if wcsa == 1 or wcsa == 3 then
      wcsa = wcsa - 1
    end
  elseif to == 1 or from == 1 then
    -- white queen-side
    if wcsa == 2 or wcsa == 3 then
      wcsa = wcsa - 2
    end
  elseif to == 64 or from == 64 then
    -- black king-side
    if bcsa == 1 or bcsa == 3 then
      bcsa = bcsa - 1
    end
  elseif to == 57 or from == 57 then
    -- black queen-side
    if bcsa == 2 or bcsa == 3 then
      bcsa = bcsa - 2
    end
  end
  board.csa = wcsa + bcsa*4
  -- 4.En passant square
  local eps = board.eps
  board.eps = 0
  -- 5.Half move clock
  board.hmc = board.hmc + 1
  if p2 or p1 <= 2 then
    board.hmc = 0
  end
  -- 6.Ply
  board.ply = board.ply + 1
  
  if p1 <= 2 then
    -- pawn move
    if to == eps then
      -- en passant capture
      local advance = stm2%2*16 - 8
      local csq = eps + advance -- todo
      board[csq] = nil
    elseif to <= 8 or to >= 57 then
      -- promotion
      -- assume we promote to queen
      p1 = promo or (p1 + 8)
    else
      -- eps square
      local dt = to - from
      if dt == 16 and from >= 9 and from <= 16 then
        board.eps = from + 8
      elseif dt == -16 and from >= 49 and from <= 56 then
        board.eps = from - 8
      end
    end
  elseif p1 >= 11 then
    -- king move
    -- castling availability and king square
    if p1 == 11 then
      board.csa = bcsa*4
      board.wks = to
    elseif p1 == 12 then
      board.csa = wcsa
      board.bks = to
    end
    -- castling (automatically move the rook)
    local dt = to - from
    if dt == 2 then
      -- king-side
      board[from + 1] = board[from + 3]
      board[from + 3] = nil
    elseif dt == -2 then
      -- queen-side
      board[from - 1] = board[from - 4]
      board[from - 4] = nil
    end
  end
  
  board[from] = nil
  board[to] = p1
end

return chess